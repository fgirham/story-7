from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.


class Story7UnitTest(TestCase):

    def test_story7_url_ada(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_story7_pakai_fungsi_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_template_yang_digunakan_benar(self):
        response = Client().get('')
        self.assertTemplateUsed(response,'story7.html')

    def test_ada_nama_irham(self):
        response = Client().get('')
        self.assertContains(response,'Irham')

    def test_ada_button_ganti_tema(self):
        response = Client().get('')
        self.assertContains(response,'Ubah Tema</button>')

    def test_ada_accordion(self):
        response = Client().get('')
        self.assertContains(response,'accordion')
    
class Story7FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story7FunctionalTest, self).setUp()


    def tearDown(self):
        self.selenium.quit()
        super(Story7FunctionalTest, self).tearDown()

    def test_ganti_tema_dan_klik_accordion(self):
        selenium = self.selenium
        
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        
        # find the element
        acc_1 = selenium.find_element_by_id('ui-id-1')
        acc_2 = selenium.find_element_by_id('ui-id-3')
        acc_3 = selenium.find_element_by_id('ui-id-5')
        button = selenium.find_element_by_id('themes')

        # test
        time.sleep(1)
        button.click()
        time.sleep(1)
        bg = selenium.find_element_by_tag_name("body").value_of_css_property("background-color")
        self.assertEqual(bg, 'rgba(20, 20, 20, 1)')
        acc_1.click()
        time.sleep(1)
        acc_1_selected = acc_1.get_attribute("aria-selected")
        self.assertEqual(acc_1_selected, 'true')
        acc_2.click()
        time.sleep(1)
        acc_2_selected = acc_2.get_attribute("aria-selected")
        self.assertEqual(acc_2_selected, 'true')
        acc_3.click()
        time.sleep(1)
        acc_3_selected = acc_3.get_attribute("aria-selected")
        self.assertEqual(acc_3_selected, 'true')
        button.click()
        time.sleep(1)
        bg = selenium.find_element_by_tag_name("body").value_of_css_property("background-color")
        self.assertEqual(bg, 'rgba(237, 237, 237, 1)')
        
        