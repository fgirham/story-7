$(document).ready(function () {
    $("#accordion").accordion({
        collapsible: true,
        active: false
    });
    var changed = true;
    $("#themes").click(function () {
        if (changed) {
            console.log("theme changed");
            $('link[href="/static/main.css"]').attr('href','/static/second.css');
            changed = false;
        } else {
            console.log("back to original");
            $('link[href="/static/second.css"]').attr('href','/static/main.css');
            changed = true;
        }

    });
});